# local import files
from serversimulator import GameServerProxy
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

from basicagent import basic_agent_factory
from cs670agent import cs670_agent_factory
from assassin import assassin_agent_factory
from matcher import matcher_agent_factory
from bestie import bestie_agent_factory

# import packages
import argparse
import numpy as np
import random

class RandomAgent(AgentSimulator):
    def __init__(self, code, base_url='http://www.juniorhighgame.com', 
            agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
            bot_reg_key=None):
        super().__init__(code, base_url, agent_name, is_player, 
            is_bot, bot_role, bot_reg_key)

    def play_round(self):
        bestie = random.choice(self.player_list)

        round_transaction = [
            {
                "receiverName": player, 
                "amount": self.round_tokens if player == bestie else 0
            } for player in self.player_list
        ]
        self.submit_tokens(round_transaction)

    def new_message(self, message):
        pass

MIN_VAL = 0.0
MAX_VAL = 500.0

def time_float_type(arg):
    try:
        f = float(arg)
    except ValueError:    
        raise argparse.ArgumentTypeError("Must be a floating point number")
    if f < MIN_VAL or f > MAX_VAL:
        raise argparse.ArgumentTypeError(f"Argument must be in range [{MIN_VAL}, {MAX_VAL}]")
    return f

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    args = parser.parse_args()

    game_code = args.code
    web_url = 'http://www.juniorhighgame.com'
    # Spawn and setup the agents
    BasicAgentSim = basic_agent_factory(is_simulated=False)
    num_basic_agents = 1
    for i in range(num_basic_agents):
        bot = BasicAgentSim(game_code, base_url=web_url, agent_name=f'basic #{i}', 
            is_player=True, is_bot=False)

    MatcherAgentSim = matcher_agent_factory(is_simulated=False)
    num_matcher_agents = 5
    for i in range(num_matcher_agents):
        bot = MatcherAgentSim(game_code, base_url=web_url, agent_name=f'matcher #{i}', 
            is_player=True, is_bot=False)

    BestieAgentSim = bestie_agent_factory(is_simulated=False)
    num_bestie_agents = 4
    for i in range(num_bestie_agents):
        bot = BestieAgentSim(game_code, base_url=web_url, agent_name=f'bestie #{i}', 
            is_player=True, is_bot=False)

    AssassinAgentSim = assassin_agent_factory(is_simulated=False)
    num_assassin_agents = 2
    for i in range(num_assassin_agents):
        bot = AssassinAgentSim(game_code, base_url=web_url, agent_name=f'assassin #{i}', 
            is_player=True, is_bot=False)

    CS670AgentSim = cs670_agent_factory(is_simulated=False)
    num_cs670_agents = 1
    for i in range(num_cs670_agents):
        bot = CS670AgentSim(game_code, base_url=web_url, agent_name=f'agent 670 #{i}', 
            is_player=True, is_bot=False)

    bot.wait()