# import local files
from agentutils import agentdecorators
from agentutils import jhgutils
from agentutils.jhgengine import JHGEngine
from simutils.graphutils import NetNode

# import packages
import time
import numpy as np
from copy import deepcopy
import random
import threading
import matplotlib
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

class GameServerProxy:
    def __init__(self):
        self.game_code = "PRXY"
        self.players = []
        self.player_registry = {}
        self.idx2player = {}
        self.player2idx = {}
        self.engine = None

        self.chat_type = jhgutils.chat_type['default']
        self.alpha = 0.2
        self.beta = 0.75
        self.give_coef = 1.4
        self.keep_coef = 1.0
        self.steal_coef = 1.1
        self.base_popularity = 100.0
        self.round_time = 120.0 #seconds per round
        self.round_num = 5

        self.curr_round = 0
        self.round_buffer = 0.13
        self.round_datastore = {}

        self.game_over = False

        self._can_process = True
    
    def _block(self):
        while not self._can_process:
            time.sleep(0.0863)
        self._can_process = False

    def _unblock(self):
        self._can_process = True

    def register(self, player):
        self.players.append(player)
        player_id = len(self.players) - 1
        self.players[player_id]._proxy_server = self
        self.players[player_id].game_code = self.game_code
        self.players[player_id]._join_game()
        client_id = self.players[player_id]._client_id
        self.player_registry[client_id] = self.players[player_id]

    def create_game(self, alpha, beta, give, keep, steal, base_popularity, 
        chat_type, round_time, round_num):
        self.alpha = alpha
        self.beta = beta
        self.give_coef = give
        self.keep_coef = keep
        self.steal_coef = steal
        self.base_popularity = base_popularity
        self.chat_type = chat_type
        self.round_time = round_time
        self.round_num = round_num

    def request(self, resource_url, params):
        # This is a glorified hack and can easily be done better
        if resource_url == jhgutils.api_resource_dict['round']:
            return self._handle_round_request(params)
        elif resource_url == jhgutils.socket_event['new_chat_message_server']:
            self._handle_new_message(params)
        elif resource_url == jhgutils.api_resource_dict['graph'] \
                or resource_url == jhgutils.api_resource_dict['graph_round']:
            return self._handle_graph_request(params)
        elif resource_url == jhgutils.api_resource_dict['submit']:
            return self._handle_submit_request(params)
        elif resource_url == jhgutils.api_resource_dict['join']:
            return self._handle_join_request(params)

    def _handle_join_request(self, join_params):
        id_chars = "ABCDEFGHIJKLMNOPQURTUVWXYZ0123456789"
        id_size = 10
        client_id = ''.join(random.choice(id_chars) for _ in range(id_size))
        return {
            "lobby": {
                "clientId": client_id,
                "chatType": self.chat_type
            }
        }

    def _handle_new_message(self, message_params):
        if self.chat_type != jhgutils.chat_type["default"]:
            chat_type = message_params.get("chatType", None)
            if chat_type == self.chat_type:
                message = message_params.get("message", None)
                if message is not None:
                    message_body = message.get("body", None)
                    player_name = message.get("playerName", None)
                    if message_body is not None and player_name is not None:
                        if self.chat_type == jhgutils.chat_type["anonymous"]:
                            player_name = "Anonymous"
                        self._broadcast_message(player_name, message_body)

    def _broadcast_message(self, sender, message):
        message_data = {
            "playerName": sender,
            "body": message
        }
        for player in self.players:
            message_thread = threading.Thread(
                target=player._on_new_chat_message,
                args=(message_data,),
                daemon=True)
            message_thread.start()

    def _handle_round_request(self, round_params):
        client_id = round_params.get("clientId", None)
        if client_id is None:
            return {}

        tokens = 2 * len(self.players)
        start_time = self.round_datastore[self.curr_round]['start_time']
        time_remaining = (self.round_time - (time.time() - start_time)) * 1000
        round_num = self.curr_round
        prev_mat = None
        if round_num == 1:
            prev_mat = np.zeros((len(self.players), len(self.players)))
        else:
            prev_round = round_num - 1
            prev_transactions = self.round_datastore[prev_round]["transactions"]
            prev_mat = self._object2Matrix(prev_transactions)

        player_name = self.player_registry[client_id].player_name
        player_idx = self.player2idx[player_name]
        given_vec = prev_mat[player_idx]
        recieved_vec = prev_mat[:, player_idx]
        recieved = []
        given = []

        for i in range(len(self.players)):
            actor_name = self.idx2player[i]
            given_amount = given_vec[i]
            given.append({
                "name": actor_name,
                "amount": given_amount
            })
            recieved_amount = recieved_vec[i]
            recieved.append({
                "name": actor_name,
                "amount": recieved_amount
            })

        round_data = self.round_datastore[self.curr_round]
        popularities = round_data['popularities']
        player_pop_data = []
        for player, pop in popularities.items():
            player_pop_data.append({
                "name": player,
                "popularity": int(round(pop))
            })

        return {"roundInfo": 
            {
                "round":round_num,
                "playerName": player_name,
                "playerPopularities": player_pop_data,
                "exchanges":{
                    "received": recieved,
                    "given": given
                },
                "numTokens": tokens,
                "timeRemaining": time_remaining, # milliseconds
                "availableGraphs": "None",
                "gameStatus": "started"
            }
        }

    def _handle_graph_request(self, graph_params):
        graph_type = graph_params.get("type", None)
        if graph_type == jhgutils.graph_types['line']:
            return self._get_line_graph()
        elif graph_type == jhgutils.graph_types['tornado']:
            round_num = graph_params.get('round', None)
            return self._get_tornado_graph(round_num=round_num)
        elif graph_type == jhgutils.graph_types['network']:
            round_num = graph_params.get('round', None)
            return self._get_network_graph(round_num=round_num)
        return []

    def _get_line_graph(self, round_num=None):
        if round_num is None:
            round_num = self.curr_round
        if round_num <= 0:
            return []
        curr_graph = self.round_datastore[round_num]['graphs']['line']
        if curr_graph is not None:
            return curr_graph
        line_graph = []
        for prev_round in range(1, round_num + 1):
            round_data = self.round_datastore[prev_round]
            popularities = round_data['popularities']
            round_pop_data = {
                "roundNum": prev_round,
                "data": []
            }
            for player, pop in popularities.items():
                round_pop_data["data"].append({
                    "name": player,
                    "value": pop
                })
            line_graph.append(round_pop_data)

        self.round_datastore[round_num]['graphs']['line'] = line_graph

        return line_graph

    def _get_tornado_graph(self, round_num=None):
        if round_num is None:
            round_num = self.curr_round
        if round_num <= 0:
            return []
        curr_graph = self.round_datastore[round_num]['graphs']['tornado']
        if curr_graph is not None:
            return curr_graph
        torn_graph = []

        round_data = self.round_datastore[round_num]
        influence = round_data['influence']
        popularities = round_data['popularities']
        for giver, reciever_data in influence.items():
            giver_data = {}
            giver_data["playerName"] = giver
            giver_data["popularity"] = popularities[giver]
            player_data = []
            for reciever, amount in reciever_data.items():
                player_data.append({
                    "name": reciever,
                    "value": amount
                })
            giver_data["playerData"] = player_data
            torn_graph.append(giver_data)
        
        self.round_datastore[round_num]['graphs']['tornado'] = torn_graph

        return torn_graph

    def _get_network_graph(self, round_num=None):
        if round_num is None:
            round_num = self.curr_round
        if round_num <= 0:
            return {}
        curr_graph = self.round_datastore[round_num]['graphs']['network']
        if curr_graph is not None:
            return curr_graph

        net_graph = {}

        # Generate the node graph
        nodes = []
        init_pos = {}
        theta = 2*np.pi / len(self.players)
        r = 1.0
        for i in range(len(self.players)):
            init_pos[i] = [r*np.cos(theta*i), r*np.sin(theta*i)]

        group_itrs = 100
        individual_itrs = 1

        influence_mat = self.engine.get_influence()
        popularity_vec = self.engine.get_popularity()

        for idx in np.argsort(popularity_vec)[::-1]:
            next_node = NetNode(
                init_pos=init_pos[idx],
                name=self.idx2player[idx], 
                code=idx, 
                popularity=popularity_vec[idx])
            nodes.append(next_node)

        if round_num >= 0:
            for _ in range(group_itrs):
                for curr_node in nodes[::-1]:
                    for _ in range(individual_itrs):
                        other_nodes = [
                            n for n in nodes if n.code != curr_node.code
                        ]
                        curr_node.update(other_nodes, influence_mat)

        if len(nodes) > 1 and False:
            positions = []
            for curr_node in nodes[::-1]:
                positions.append(curr_node.get_current_pos())
            mean_position = np.mean(np.asarray(positions), axis=0)
            std_postition = np.std(np.asarray(positions), axis=0)
            for curr_node in nodes[::-1]:
                curr_node.position.append(
                    (curr_node.position[-1] - mean_position) / std_postition
                )

        nodes.sort(key=lambda x: x.code)

        node_data = []
        for node in nodes:
            pos = node.get_current_pos()
            node_data.append({
                "name": node.name,
                "x": pos[0],
                "y": pos[1]
            })

        net_graph["nodeData"] = node_data

        edge_mat = influence_mat + influence_mat.T
        np.fill_diagonal(edge_mat, 0.0)
        max_edge = np.amax(np.abs(edge_mat))
        if max_edge != 0.0:
            edge_mat /= max_edge

        edge_data = []
        for i in range(len(self.players)):
            for j in range(i, len(self.players)):
                edge_data.append({
                    "n1": self.idx2player[i],
                    "n2": self.idx2player[j],
                    "value": edge_mat[i, j]
                })

        net_graph["edgeData"] = edge_data

        self.round_datastore[round_num]['graphs']['network'] = net_graph

        return net_graph

    def _handle_submit_request(self, submit_params):
        self._block()
        client_id = submit_params.get("clientId", None)
        if client_id is None or client_id not in self.player_registry.keys():
            return
        player = self.player_registry[client_id]

        giver_name = player.player_name
        round_num = submit_params.get("round", -1)
        round_data = self.round_datastore.get(round_num, None)
        if round_data is None:
            self._unblock()
            return
        round_end_time = round_data["end_time"]
        if round_end_time is not None:
            # round has ended so ignore this request
            self._unblock()
            return
        if client_id in self.round_datastore[round_num]["submitted_ids"]:
            # player has already submitted
            self._unblock()
            return
        submission = submit_params.get("submission", None)
        if submission is None:
            self._unblock()
            return
        token_assignments = submission.get("tokenAssignments", None)
        is_valid, clean_tkns = self._validate_tokens(token_assignments)
        
        if not is_valid:
            self._unblock()
            return
        self.round_datastore[round_num]["transactions"][giver_name] = clean_tkns
        self.round_datastore[round_num]["submitted_ids"].append(client_id)
        submitted_cnt = len(self.round_datastore[round_num]["submitted_ids"])
        print(f'{submitted_cnt} of {len(self.players)} players have submitted for round {round_num}')
        if submitted_cnt == len(self.players):
            self._unblock()
            self.process_round(round_num)
        else:
            self._unblock()
        
    def _validate_tokens(self, tokens):
        if not isinstance(tokens, list):
            return False, []
        total_tokens = 0
        clean_submission = []
        for token_alloc in tokens:
            if not isinstance(token_alloc, dict):
                return False, []
            reciever_name = token_alloc.get("receiverName", None)
            raw_amount = token_alloc.get("amount", None)
            if reciever_name is None or raw_amount is None:
                return False, []
            int_amount = int(raw_amount)
            clean_submission.append({
                "receiverName": reciever_name,
                "amount": int_amount
            })
            total_tokens += abs(int_amount)
        
        if total_tokens > 2 * len(self.players):
            return False, []
        return True, clean_submission

    def start_game(self):
        random.shuffle(self.players)
        for i, player in enumerate(self.players):
            player.player_name = f"Player {i}"
            self.idx2player[i] = player.player_name
            self.player2idx[player.player_name] = i

        self.engine = JHGEngine(alpha=self.alpha, beta=self.beta, 
            give=self.give_coef, keep=self.keep_coef, steal=self.steal_coef, 
            base_popularity=self.base_popularity, num_players=len(self.players))

        self.curr_round = 1

        self._setup_round()

        for player in self.players:
            start_thread = threading.Thread(target=player._on_start_game,
                daemon=True)
            start_thread.start()

    def _setup_round(self):
        print(f"setting up datastore for round {self.curr_round}")
        self.round_datastore[self.curr_round] = {
            "timer": None,
            "start_time": None,
            "end_time": None,
            "transactions": self._generate_transactions(),
            "submitted_ids": [],
            "popularities": None,
            "influence": None,
            "graphs": {
                "line": None,
                "network": None,
                "tornado": None
            }
        }

        popularity_vec = self.engine.get_popularity()
        popularity_obj = {}
        for player_idx in range(len(self.players)):
            player_name = self.idx2player[player_idx]
            popularity_obj[player_name] = popularity_vec[player_idx]
        
        self.round_datastore[self.curr_round]['popularities'] = popularity_obj
        
        influence_mat = self.engine.get_influence()
        influence_obj = self._matrix2Object(influence_mat)
        self.round_datastore[self.curr_round]['influence'] = influence_obj

        self.round_datastore[self.curr_round]['timer'] = threading.Timer(
            self.round_time + self.round_buffer, self.process_round, 
            args=(self.curr_round,))

        print(f'\n===============\nround {self.curr_round}\n===============\n')
        
        self.round_datastore[self.curr_round]['timer'].start()
        self.round_datastore[self.curr_round]['start_time'] = time.time()

    def _generate_transactions(self):
        tokens = 2 * len(self.players)
        players = list(self.idx2player.values())
        return {
            giver_name: [
                {
                    "receiverName": receiver_name, 
                    "amount": tokens if giver_name == receiver_name else 0
                } for receiver_name in players
            ] for giver_name in players
        }

    def _matrix2Object(self, mat):
        obj = {}
        for giver_idx in range(len(self.players)):
            giver_name = self.idx2player[giver_idx]
            obj[giver_name] = {}
            for reciever_idx in range(len(self.players)):
                reciever_name = self.idx2player[reciever_idx]
                obj[giver_name][reciever_name] = mat[giver_idx, reciever_idx]
        return obj

    def _object2Matrix(self, obj):
        mat = np.zeros((len(self.players), len(self.players)))
        for giver_name, transactions in obj.items():
            giver_idx = self.player2idx[giver_name]
            if giver_name is not None:
                for transaction in transactions:
                    reciever_name = transaction['receiverName']
                    reciever_idx = self.player2idx[reciever_name]
                    mat[giver_idx, reciever_idx] = int(transaction['amount'])
        return mat

    def process_round(self, round_num):
        if self.round_datastore[round_num]['end_time'] is None:
            self.round_datastore[round_num]['end_time'] = time.time()
            self._block()
            self.round_datastore[round_num]['timer'].cancel()
            print(f"processing round {self.curr_round}")

            transactions = self.round_datastore[round_num]['transactions']
            transaction_matrix = self._object2Matrix(transactions)

            self.engine.apply_transaction(transaction_matrix)

            self.curr_round += 1
            self._setup_round()

            '''
            for sub_id in self.round_datastore[round_num]["submitted_ids"]:
                print(f'{self.player_registry[sub_id].agent_name} submitted')
            '''

            if self.curr_round <= self.round_num:
                for player in self.players:
                    end_round_thread = threading.Thread(
                        target=player._on_end_round, 
                        args=(None,),
                        daemon=True)
                    end_round_thread.start()
            else:
                self.round_datastore[self.curr_round]['timer'].cancel()
                for player in self.players:
                    end_game_thread = threading.Thread(
                        target=player._on_game_over, 
                        args=(None,),
                        daemon=True)
                    end_game_thread.start()
                print('GAME OVER')
                self._print_player_names()
                self.game_over = True
            self._unblock()
            
    def _print_player_names(self):
        popularity_obj = self.round_datastore[self.curr_round]['popularities']
        for player in self.players:
            player_popularity = popularity_obj[player.player_name]
            print(f'{player.player_name} was {player.agent_name} ' + \
                f'with a final popularity of {player_popularity}')

    def wait(self):
        while not self.game_over:
            time.sleep(0.13)

    def show_network_graph(self, round_num):
        colors = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", 
                    "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", 
                    "#ffff99", "#b15928", "#8dd3c7", "#80b1d3", "#b3de69", 
                    "#fccde5", "#d9d9d9", "#ffed6f", "#bc80bd", "#999999", 
                    "#1ae3be", "#1f3fb4", "#e31a9a", "#e3861a", "#e16b6c", 
                    "#4f4f4f"]
        
        if round_num <= 0 or round_num >= self.curr_round:
            return

        datastore = self.round_datastore[round_num]

        fig, ax = plt.subplots(figsize=(7, 7))

        adj_mat = np.zeros((len(self.player2idx), len(self.player2idx)))
        if datastore['graphs']['network'] is None:
            self._get_network_graph(round_num)
            datastore = self.round_datastore[round_num]
        
        for edge in datastore['graphs']['network']['edgeData']:
            giver_name = edge['n1']
            giver_idx = self.player2idx[giver_name]
            receiver_name = edge['n2']
            receiver_idx = self.player2idx[receiver_name]
            value = edge['value']
            adj_mat[giver_idx, receiver_idx] = value

        max_x = float('-inf')
        min_x = float('inf')
        max_y = float('-inf')
        min_y = float('inf')
        max_size = 0

        circle_markers = []
        marker_names = []

        popularity_obj = datastore['popularities']
        max_pop = np.amax(list(popularity_obj.values()))

        for node in datastore['graphs']['network']['nodeData']:
            node_name = node['name']
            node_idx = self.player2idx[node_name]

            pop = popularity_obj[node_name]
            if pop == 0:
                node_size = 0.01
            else:
                node_size = pop / max_pop

            circle = plt.Circle((node['x'], node['y']), 
                max(pop/max_pop/10, 0.01), 
                label="{} @{}".format(node_name, np.round(pop)),
                color=colors[node_idx % len(colors)], 
                fill=pop > 0, alpha=0.75)
            
            ax.add_artist(circle)
            ax.annotate("{}".format(node_name),
                (node['x'], node['y']), va="top", ha="center")
            marker_names.append(f"{node_name} @{np.round(pop)}")

            max_x = max(max_x, node['x'])
            min_x = min(min_x, node['x'])
            max_y = max(max_y, node['y'])
            min_y = min(min_y, node['y'])
            max_size = max(max_size, circle.radius)

        for giver in datastore['graphs']['network']['nodeData']:
            giver_name = giver['name']
            giver_idx = self.player2idx[giver_name]
            for receiver in datastore['graphs']['network']['nodeData']:
                receiver_name = receiver['name']
                receiver_idx = self.player2idx[receiver_name]    
                if adj_mat[giver_idx, receiver_idx] != 0:
                    if adj_mat[giver_idx, receiver_idx] < 0:
                        line_color = 'r'
                    else:
                        line_color = 'g'
                    rel = adj_mat[giver_idx, receiver_idx]

                    style=f"Simple,tail_width={rel},head_width=4,head_length=8"
                    kw = dict(arrowstyle=style)

                    start_pos = (giver['x'], receiver['x'])
                    end_pos = (giver['y'], receiver['y'])
                    
                    line = plt.Line2D(start_pos, end_pos, color=line_color, 
                        alpha=min(1.0, abs(rel) / np.amax(np.abs(adj_mat))))
                    ax.add_artist(line)


        ax.set_xlim(xmin=min(min_x, min_y) - 2*max_size, 
            xmax=max(max_x, max_y) + 2*max_size)
        ax.set_ylim(ymin=min(min_x, min_y) - 2*max_size, 
            ymax=max(max_x, max_y) + 2*max_size)

        ax.set_title(f"Network Graph\nRound {round_num}")
        ax.grid(True)
        legend = ax.legend(circle_markers, marker_names, 
            loc='center left', bbox_to_anchor=(1, 0.5))
        
        for i in range(len(legend.legendHandles)):
            legend.legendHandles[i]._sizes = [100]

        plt.show()

    def show_popularity_graph(self, round_num):
        colors = ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", 
                    "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", 
                    "#ffff99", "#b15928", "#8dd3c7", "#80b1d3", "#b3de69", 
                    "#fccde5", "#d9d9d9", "#ffed6f", "#bc80bd", "#999999", 
                    "#1ae3be", "#1f3fb4", "#e31a9a", "#e3861a", "#e16b6c", 
                    "#4f4f4f"]
        
        if round_num <= 0 or round_num >= self.curr_round:
            return

        datastore = self.round_datastore[round_num]
        if datastore['graphs']['line'] is None:
            self._get_line_graph(round_num)
            datastore = self.round_datastore[round_num]
        rounds = []
        player_pops = {}
        for round_data in datastore['graphs']['line']:
            rounds.append(round_data['roundNum'])
            for player_data in round_data['data']:
                player_name = player_data['name'] 
                popularity = player_data['value']
                player_vec = player_pops.get(player_name, [])
                player_vec.append(popularity)
                player_pops[player_name] = player_vec

        _, ax = plt.subplots(figsize=(7, 7))

        for idx, (player_name, popularity) in enumerate(player_pops.items()):
            ax.plot(rounds, popularity, label=player_name, c=colors[idx % len(colors)])

        ax.set_title(f"Popularity Chart\nRound {round_num}")
        ax.set_xlabel('Round #')
        ax.set_ylabel('Popularity')
        ax.grid(True)
        ax.legend()
        plt.show()

    def show_tornado_graph(self, round_num):
        if round_num <= 0 or round_num >= self.curr_round:
            return

        datastore = self.round_datastore[round_num]
        if datastore['graphs']['tornado'] is None:
            self._get_tornado_graph(round_num)
            datastore = self.round_datastore[round_num]
        influence_mat = np.zeros((len(self.player2idx), len(self.player2idx)))

        for player_data in datastore['graphs']['tornado']:
            player_name = player_data['playerName']
            player_idx = self.player2idx[player_name]
            for other_data in player_data['playerData']:
                other_name = other_data['name']
                other_idx = self.player2idx[other_name]
                influence = other_data['value']
                influence_mat[player_idx, other_idx] = influence

        fig, ax = plt.subplots(figsize=(7, 7))

        plt.text(0.5, -0.08, f"Tornado Graph\nRound {round_num}",
            horizontalalignment='center',
            fontsize=12,
            transform = ax.transAxes)

        cb = ax.matshow(influence_mat, cmap='coolwarm')

        ax.set_xticks(np.arange(len(influence_mat)))
        ax.set_yticks(np.arange(len(influence_mat)))
        ax.set_ylim(len(influence_mat)-0.5, -0.5)

        labels = list(self.player2idx.keys())

        ax.set_xticklabels(labels, rotation=90, fontsize=8)
        ax.set_yticklabels(labels, fontsize=8)

        ax.set_xlabel("Reciever", fontsize=8)
        ax.set_ylabel("Giver", fontsize=8)

        ax.xaxis.set_label_coords(0.5, 1.15)
        ax.yaxis.set_label_coords(-0.15, 0.5)

        ax.figure.colorbar(cb, ax=ax)
        for i in range(len(influence_mat)):
            for j in range(len(influence_mat)):
                c = influence_mat[j, i]
                ax.text(i, j, "{:.2f}".format(c), va='center', ha='center', 
                    fontsize=8, rotation=45)
        plt.show()

    def show_transaction_graph(self, round_num):
        if round_num <= 0 or round_num >= self.curr_round:
            return

        datastore = self.round_datastore[round_num]

        transaction_mat = np.zeros((len(self.player2idx), len(self.player2idx)))

        for player_name, actions in datastore['transactions'].items():
            player_idx = self.player2idx[player_name]
            for alloc in actions:
                other_name = alloc['receiverName']
                other_idx = self.player2idx[other_name]
                tokens = alloc['amount']
                transaction_mat[player_idx, other_idx] = tokens

        fig, ax = plt.subplots(figsize=(7, 7))

        plt.text(0.5, -0.08, f"Transaction Matrix\nRound {round_num}",
            horizontalalignment='center',
            fontsize=12,
            transform = ax.transAxes)

        cb = ax.matshow(transaction_mat, cmap='coolwarm')

        ax.set_xticks(np.arange(len(transaction_mat)))
        ax.set_yticks(np.arange(len(transaction_mat)))
        ax.set_ylim(len(transaction_mat)-0.5, -0.5)

        labels = list(self.player2idx.keys())

        ax.set_xticklabels(labels, rotation=90, fontsize=8)
        ax.set_yticklabels(labels, fontsize=8)

        ax.set_xlabel("Reciever", fontsize=8)
        ax.set_ylabel("Giver", fontsize=8)

        ax.xaxis.set_label_coords(0.5, 1.15)
        ax.yaxis.set_label_coords(-0.15, 0.5)

        ax.figure.colorbar(cb, ax=ax)
        for i in range(len(transaction_mat)):
            for j in range(len(transaction_mat)):
                c = transaction_mat[j, i]
                ax.text(i, j, "{:.2f}".format(c), va='center', ha='center', 
                    fontsize=8, rotation=45)
        plt.show()
