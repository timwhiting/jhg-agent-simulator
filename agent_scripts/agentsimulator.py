# import local files
from agentutils import jhgutils
from agentutils import agentdecorators

# import packages
import numpy as np

class AgentSimulator:
    def __init__(self, code, base_url='http://www.juniorhighgame.com', agent_name='ai', 
            is_player=True, is_bot=True, bot_role=None, bot_reg_key=None):
        self._proxy_server = None

        self._client_id = None
        
        self.game_code = code
        self.agent_name = agent_name # Name shown at game over

        self._is_player = is_player
        if self._is_player:
            self._player_role = jhgutils.game_roles['player']
        else:
            self._player_role = jhgutils.game_roles['observer']
        
        self.player_list = None
        self.player2idx = None
        self.idx2player = None

        self.player_name = 'Navi' # Name assigned by the server

        self.round_data = {}
        self.round_num = -1
        self.round_time = -1.0
        self.round_tokens = 0

        self.chat_type = 'Default'
    
    def _on_start_game(self):
        self._process_round()

    def _on_game_over(self, data):
        pass

    def _on_new_chat_message(self, data):
        self.new_message(data)

    def _on_end_round(self, data):
        self._process_round()

    def _join_game(self):
        join_params = {
            "code": self.game_code,
            "isPlayer": self._is_player,
            "playerExperience": "0 to 1 games played",
            "userName": self.agent_name
        }

        response_data = self._proxy_server.request(
            resource_url=jhgutils.api_resource_dict['join'], 
            params=join_params
        )

        self._client_id = response_data['lobby']['clientId']
        self.chat_type = response_data['lobby']['chatType']

        socket_data = {
            "code": self.game_code,
            "id": self._client_id,
            "role": self._player_role
        }

        #self._proxy_server.request(jhgutils.socket_event['joined_game'], socket_data)
   
    def send_message(self, message):
        message_data = {
            "message": {
                "playerName": self.player_name,
                "body": message
            },
            "chatType": self.chat_type
        }

        response_data = self._proxy_server.request(
            resource_url=jhgutils.socket_event['new_chat_message_server'], 
            params=message_data
        )
    
    def get_current_round(self):
        round_params = {
            "code": self.game_code,
            "clientId": self._client_id
        }

        response_data = self._proxy_server.request(
            resource_url=jhgutils.api_resource_dict['round'], 
            params=round_params
        )

        self.round_num = response_data['roundInfo']['round']
        self.round_data[self.round_num] = response_data['roundInfo']
        self.round_tokens = self.round_data[self.round_num]['numTokens']
        self.player_name = self.round_data[self.round_num]['playerName']

        if self.player_list is None:
            exchanges_given = response_data['roundInfo']['exchanges']['given']
            
            self.player_list = [
                given_data['name'] for given_data in exchanges_given
            ]

            self.player2idx = {
                player: idx for idx, player in enumerate(self.player_list)
            }

            self.idx2player = {
                idx: player for idx, player in enumerate(self.player_list)
            }
    
    def get_line_graph(self):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['line']
        )

        return self._parse_line_graph(raw_data)

    def get_tornado_graph(self, round_num=None, matrix_format=False):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['tornado'], 
            round_num
        )

        return self._parse_tornado_graph(raw_data, matrix_format)

    def get_network_graph(self, round_num=None):
        raw_data = self._get_graph_data(
            jhgutils.graph_types['network'], 
            round_num
        )

        return self._parse_network_graph(raw_data)

    def _get_graph_data(self, graph_type, round_num=None):
        if round_num is None:
            round_num = self.round_num

        get_params = {
            "code": self.game_code,
            "type": graph_type,
        }

        if graph_type != jhgutils.graph_types['line']:
            get_params["round"] = round_num
            resource_key = 'graph_round'
        else:
            resource_key = 'graph'

        response_data = self._proxy_server.request(
            resource_url=jhgutils.api_resource_dict[resource_key], 
            params=get_params
        )

        return response_data

    def _clean_data(self, data, remove_list):
        if isinstance(data, dict):
            for remove_key in remove_list:
                if remove_key in data.keys():
                    del data[remove_key]
                    
            for key in data.keys():
                self._clean_data(data[key], remove_list)
        elif isinstance(data, list):
            for el in data:
                self._clean_data(el, remove_list)

    def _parse_tornado_graph(self, tornado_data, matrix_format=False):
        cleaned_data = tornado_data
        
        if matrix_format:
            num_players = len(self.player_list)
            tornado_mat = np.zeros((num_players, num_players))

            for player_data in cleaned_data:
                giver_idx = self.player2idx[player_data['playerName']]
                for interaction in player_data['playerData']:
                    reciever_idx = self.player2idx[interaction['name']] 
                    tornado_mat[giver_idx, reciever_idx] = interaction['value']

            return tornado_mat
        else:
            return cleaned_data

    def _parse_line_graph(self, line_data):
        return line_data

    def _parse_network_graph(self, network_data):
        return network_data

    def submit_tokens(self, transaction):
        post_params = {
            "code": self.game_code,
            "clientId": self._client_id,
            "round": self.round_num,
            "submission": {
                "tokenAssignments": transaction
            }
        }

        response_data = self._proxy_server.request(
            resource_url=jhgutils.api_resource_dict['submit'], 
            params=post_params
        )

    def _process_round(self):
        self.get_current_round()

        self.play_round()

    def play_round(self):
        if self.chat_type != jhgutils.chat_type['free']:
            self.send_message("2 tkns to everyone")
        elif self.chat_type != jhgutils.chat_type['anonymous']:
            self.send_message("2 tkns to everyone -{}".format(self.player_name))
        
        round_transaction = [
            {"receiverName": player, "amount": 2} for player in self.player_list
        ]
        self.submit_tokens(round_transaction)

    def new_message(self, message):
        pass
