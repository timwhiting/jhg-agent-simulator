# import local files
from copy import deepcopy
import random
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils
from queue import Queue
# import packages
import argparse
import time
import numpy as np


def bestiedestroyer_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class BestieDestroyer(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='bestie_destroyer', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None):
            super().__init__(code, base_url, agent_name, is_player, 
                is_bot, bot_role, bot_reg_key)
            self.memory = 1
            self.num_besties = 2
            self.known_enemies = set()
            self.tornado_matrix = None

        def play_round(self):
            print(self.player_name)
            # TODO: Change this for how you want your agent to act
            self.last_tornado = self.tornado_matrix
            self.tornado_matrix = self.get_tornado_graph(matrix_format=True)
            self.start_time = time.time()

            if self.round_num == 1:
                player_list_copy = deepcopy(self.player_list)
                random.shuffle(player_list_copy)
                besties = player_list_copy[:self.num_besties]
                coalition = 'Coalition:'
                for p in besties:
                    coalition += ' @' + p + ';'
                self.send_message(coalition)

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": self.round_tokens//self.num_besties if player in besties else 0
                    } for player in self.player_list
                ]
            else:
                for _ in range(min(1, self.round_num - self.memory), self.round_num + 1):
                    exchanges = self.round_data[self.round_num]['exchanges']
                    received_lookup = {}
                    for received in exchanges['received']:
                        if received['name'] != self.player_name:
                            prev_amount = received_lookup.get(received['name'], 0.0)
                            received_lookup[received['name']] = received['amount'] + prev_amount
                        else:
                            received_lookup[received['name']] = 0.0
                        
                if self.last_tornado is not None:
                    tornado_diff = self.tornado_matrix - self.last_tornado
                    bad_classification = -np.max(np.abs(tornado_diff))*2/3
                    bad_interactions = tornado_diff < bad_classification
                    for attacker, interactions in enumerate(bad_interactions):
                        for person, attacked in enumerate(interactions):
                            if attacked.item():
                                if person in self.known_enemies:
                                    continue
                                else:
                                    print('Attacker {} attacked {} who was not a known enemy'.format(self.idx2player[attacker], self.idx2player[person]))
                                    if person is not self.player2idx[self.player_name]:
                                        self.known_enemies.add(attacker)


                remove_list = set()
                for pop in self.round_data[self.round_num]['playerPopularities']:
                    if pop['popularity'] <= 10:
                        remove_list.add(self.player2idx[pop['name']])
                print('Dead players {}'.format(remove_list))
                self.known_enemies = self.known_enemies.difference(remove_list)
                print('Known enemies {}'.format(self.known_enemies))
                

                alloc_vec = np.zeros(len(self.player_list))
                
                kill_tokens = min(self.round_tokens*len(self.known_enemies)/4, self.round_tokens)
                if kill_tokens > 0:
                    en = list(self.known_enemies)[0]
                    alloc_vec[en] = -kill_tokens // len(self.known_enemies)
                    self.send_message('Attack: @{}'.format(self.idx2player[en]))
                
                for player in self.player_list:
                    player_idx = self.player2idx[player]
                    player_tokens = received_lookup[player]
                    alloc_vec[player_idx] += player_tokens ** 2 * np.sign(player_tokens)

                if np.sum(np.abs(alloc_vec)) != 0:
                    # normalize the allocation vector using L1 norm
                    token_vec = np.sign(alloc_vec) \
                        * np.round(np.abs(alloc_vec) / np.sum(np.abs(alloc_vec)) \
                            * self.round_tokens)
                else:
                    token_vec = np.zeros(len(self.player_list))
                    player_list_copy = deepcopy(self.player_list)
                    random.shuffle(player_list_copy)
                    besties = player_list_copy[:self.num_besties]
                    for bestie in besties:
                        token_vec[self.player2idx[bestie]] = self.round_tokens // self.num_besties
                    tokens_used = np.sum(np.abs(token_vec))
                    coalition = 'Coalition:'
                    for p in besties:
                        coalition += ' @' + p + ';'
                    self.send_message(coalition)

                tokens_used = np.sum(np.abs(token_vec))

                while tokens_used != self.round_tokens:
                    if tokens_used > self.round_tokens:
                        # take tokens from the lowest
                        min_idx = np.argmax(np.abs(token_vec[np.nonzero(tokens_used)]))
                        min_sign = np.sign(token_vec)[min_idx]
                        indices = np.argwhere(token_vec == token_vec[min_idx]).flatten()
                        change_idx = random.choice(indices)
                        token_vec[change_idx] -= min_sign
                    elif tokens_used < self.round_tokens:
                        if len(self.known_enemies) > 0:
                            token_vec[random.choice(list(self.known_enemies))] -= 1
                        else:
                            # add tokens to the highest
                            max_idx = np.argmax(np.abs(token_vec))
                            max_sign = np.sign(token_vec)[max_idx]
                            indices = np.argwhere(token_vec == token_vec[max_idx]).flatten()
                            change_idx = random.choice(indices)
                            token_vec[change_idx] += max_sign
                    tokens_used = np.sum(np.abs(token_vec))

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": token_vec[self.player2idx[player]]
                    } for player in self.player_list
                ]
  
            self.submit_tokens(round_transaction)
        
        def new_message(self, message):
            pass
            # self.message_queue.put(message)

    return BestieDestroyer

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    BestieOnline = bestiedestroyer_factory(is_simulated=False)

    bot = BestieOnline(args.code, base_url='http://www.juniorhighgame.com', 
        agent_name='bestie_destroyer', is_player=True, is_bot=False)
    
    bot.wait()