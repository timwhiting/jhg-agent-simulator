import numpy as np

class JHGEngine():

    def __init__(self, alpha=0.2, beta=0.75, give=1.4, keep=1.0, steal=1.1, 
        num_players=3, base_popularity=100.0):
        # Scalars
        ## Weight the value of previous exchanges
        self.alpha = alpha

        ## Weights the value of previous popularity scores
        self.beta = beta

        ## scaling coefficient for tokens given, kept and stolen
        self.C_g = give
        self.C_k = keep
        self.C_s = steal

        ## total number of players
        self.N = num_players

        ## Popularity of player i at time t
        self.P = []
        self.P.append(np.ones(self.N) * base_popularity)

        ## current round number
        self.t = 0

        ## historical transaction matrices
        self.T = []
        self.T.append(np.zeros((self.N, self.N)))

        ## influence matrices
        self.I = []
        self.I.append(np.zeros((self.N, self.N))) # i = influences => j

        ## Death Policy
        self.zero_death = True

    # Functions
    ## Recurrence relation for storing the previous rounds' populatiry
    def F(self, tau, t, i):
        if tau == 0:
            return self.P[0][i]

        F_i = self.F(tau - 1, t, i)
        G_i = self.G(tau, t, i)
        return self.alpha * G_i + (1 - self.alpha) * F_i

    def I_func(self, tau, t, i, j):
        if tau == 0:
            return 0.0
        I_hat_out = self.I_hat_func(tau, t, i, j)
        return (1 - self.alpha) * self.I_func(tau - 1, t, i, j) + self.alpha * (1.0 / (2.0 * self.N)) * I_hat_out

    def I_hat_func(self, tau, t, i, j):
        if i == j:
            V_out = self.C_k * self.W(tau, t, i) * self.T[tau][i, i]
            for k in range(self.N):
                if self.T[tau][i, k] < 0:
                    V_out += self.C_S(tau, t, k) * self.W(tau, t, i) * np.abs(self.T[tau][i, k])
            return V_out
        else:
            V_out = 0.0
            if self.T[tau][j, i] > 0.0:
                V_ji = self.C_g * self.W(tau, t, j) * self.T[tau][j, i]
                V_out = V_out + V_ji
            
            if self.T[tau][j, i] < 0.0:
                C_s_i = self.C_S(tau, t, i)
                V_ij = C_s_i * self.W(tau, t, j) * np.abs(self.T[tau][j, i])
                V_out = V_out - V_ij

            return V_out

    ## Net popularity flow to player i at time tau for rounds tau - 1 and t - 1
    def G(self, tau, t, i):
        G_out = 0.0
        I_t = np.zeros((self.N, self.N))
        for j in range(self.N):
            v_j, I_tau = self.V(tau, t, i, j)
            G_out = G_out + v_j
            I_t = I_t + I_tau
        self.I[t][:, i] = (1. - self.alpha) * self.I[t][:, i] + I_t[:, i]
        return (1.0 / (2.0 * self.N)) * G_out

    ## net popularity flow from player i to j at time tau
    def V(self, tau, t, i, j):
        I_tau = np.zeros((self.N, self.N))
        if i == j:
            V_out = self.C_k * self.W(tau, t, i) * self.T[tau][i, i]
            I_tau[j, i] = I_tau[j, i] + self.alpha * V_out * (1.0 / (2.0 * self.N))
            return V_out, I_tau
        else:
            V_out = 0.0
            if self.T[tau][j, i] > 0.0:
                V_ji = self.C_g * self.W(tau, t, j) * self.T[tau][j, i]
                I_tau[j, i] = I_tau[j, i] + self.alpha * V_ji * (1.0 / (2.0 * self.N))
                V_out = V_out + V_ji
            elif self.T[tau][j, i] < 0:
                C_s_j = self.C_S(tau, t, i)
                V_ji = C_s_j * self.W(tau, t, j) * np.abs(self.T[tau][j, i])
                I_tau[j, i] = I_tau[j, i] + self.alpha * V_ji * (-1.0 / (2.0 * self.N))
                V_out = V_out - V_ji
            
            if self.T[tau][i, j] < 0.0:
                C_s_i = self.C_S(tau, t, j)
                V_ij = C_s_i * self.W(tau, t, i) * np.abs(self.T[tau][i, j])
                I_tau[i, i] = I_tau[i, i] + self.alpha * V_ij * (1.0 / (2.0 * self.N))
                V_out = V_out + V_ij

            return V_out, I_tau
    
    def C_S(self, tau, t, i):
        TW_ki = 0.0
        T_ii = self.T[tau][i, i] if self.T[tau][i, i] >= 0.0 else 0.0
        for k in range(self.N):
            if self.T[tau][k, i] <= 0.0:
                TW_ki = TW_ki + np.abs(self.T[tau][k, i]) * self.W(tau, t, k)

        if TW_ki == 0.0:
            return TW_ki
        else:
            return self.C_s * np.maximum(1.0 - (T_ii * self.W(tau, t, i)) / TW_ki, 0.0)     

    ## weight of player's actions at time tau based on i's populatiry at time tau - 1 and t - 1
    def W(self, tau, t, i):
        return self.beta * self.P[tau - 1][i] + (1 - self.beta) * self.P[t - 1][i]

    def apply_transaction(self, T):
        self.t += 1
        self.T.append(T)
        self.I.append(np.zeros((self.N, self.N))) # i = influences => j
        next_P = np.zeros_like(self.P[-1])
        for i in range(self.N):
            next_P[i] = self.F(self.t, self.t, i)
        self.P.append(next_P)
        if self.zero_death:
            self.P[self.t][self.P[self.t] < 0] = 0.0

    def get_influence(self, t=None):
        return self.I[t] if t is not None else self.I[self.t]

    def get_popularity(self):
        return self.P[self.t]