# import local files
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

# import packages
import argparse
import random
import numpy as np
from copy import deepcopy

def bestie_agent_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class BestieAgent(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None):
            super().__init__(code, base_url, agent_name, is_player, is_bot, 
                bot_role, bot_reg_key)
            self.memory = 1
            self.num_besties = 2

        def play_round(self):
            if self.round_num == 1:
                player_list_copy = deepcopy(self.player_list)
                random.shuffle(player_list_copy)
                besties = player_list_copy[:self.num_besties]

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": self.round_tokens//self.num_besties if player in besties else 0
                    } for player in self.player_list
                ]
            else:
                for round_num in range(min(1, self.round_num - self.memory), self.round_num + 1):
                    exchanges = self.round_data[self.round_num]['exchanges']
                    received_lookup = {}
                    for received in exchanges['received']:
                        if received['name'] != self.player_name:
                            prev_amount = received_lookup.get(received['name'], 0.0)
                            received_lookup[received['name']] = received['amount'] + prev_amount
                        else:
                            received_lookup[received['name']] = 0.0
                
                alloc_vec = np.zeros(len(self.player_list))
                for player in self.player_list:
                    player_idx = self.player2idx[player]
                    player_tokens = received_lookup[player]
                    alloc_vec[player_idx] = player_tokens ** 2 * np.sign(player_tokens)

                if np.sum(np.abs(alloc_vec)) != 0:
                    # normalize the allocation vector using L1 norm
                    token_vec = np.sign(alloc_vec) \
                        * np.round(np.abs(alloc_vec) / np.sum(np.abs(alloc_vec)) \
                            * self.round_tokens)
                else:
                    token_vec = np.zeros(len(self.player_list))
                    player_list_copy = deepcopy(self.player_list)
                    random.shuffle(player_list_copy)
                    besties = player_list_copy[:self.num_besties]
                    for bestie in besties:
                        token_vec[self.player2idx[bestie]] = self.round_tokens // self.num_besties
                    tokens_used = np.sum(np.abs(token_vec))

                tokens_used = np.sum(np.abs(token_vec))

                while tokens_used != self.round_tokens:
                    if tokens_used > self.round_tokens:
                        # take tokens from the lowest
                        min_idx = np.argmax(np.abs(token_vec[np.nonzero(tokens_used)]))
                        min_sign = np.sign(token_vec)[min_idx]
                        indices = np.argwhere(token_vec == token_vec[min_idx]).flatten()
                        change_idx = random.choice(indices)
                        token_vec[change_idx] -= min_sign
                    elif tokens_used < self.round_tokens:
                        # add tokens to the highest
                        max_idx = np.argmax(np.abs(token_vec))
                        max_sign = np.sign(token_vec)[max_idx]
                        indices = np.argwhere(token_vec == token_vec[max_idx]).flatten()
                        change_idx = random.choice(indices)
                        token_vec[change_idx] += max_sign
                    tokens_used = np.sum(np.abs(token_vec))

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": token_vec[self.player2idx[player]]
                    } for player in self.player_list
                ]

            self.submit_tokens(round_transaction)
    
    return BestieAgent

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    BestieOnlineAgent = bestie_agent_factory(is_simulated=False)

    bot = BestieOnlineAgent(args.code, base_url='http://www.juniorhighgame.com', 
        agent_name='bestie bot #1', is_player=True, is_bot=False)
        
    bot.wait()
    